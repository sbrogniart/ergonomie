<!DOCTYPE HTML>
<html>
	<head>
		<title>Photolocate</title>
	  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
	  <script src="bootstrap-sass-3.3.2/assets/javascripts/bootstrap/transition.js"></script>
	  <script src="//cdn.ckeditor.com/4.4.7/standard/ckeditor.js"></script>
	  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	  	 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	  	 <script>
		    $( "form" ).submit(function( event ) {
			  $("#alert").show( "slow" ).fadeOut(3000);
			  event.preventDefault();
			});
	  </script>
	  <link rel="stylesheet" type="text/css" href="scss/screen.css">
	</head>

	<body>
		<header>
			<nav class="navbar navbar-default">
				  <div class="container-fluid">
					    <!-- Brand and toggle get grouped for better mobile display -->
					    <div class="navbar-header">
					      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					        <span class="sr-only">Toggle navigation</span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					      </button>
					      <a class="navbar-brand" href="#">Menu</a>
					    </div>

					    <!-- Collect the nav links, forms, and other content for toggling -->
					    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					      <ul class="nav navbar-nav">
					        <li class="active"><a href="index.php">Accueil <span class="sr-only">(current)</span></a></li>
					        <li><a href="upload.php">Ajouter une actualité</a></li>
					        <li><a href="connexion.php">Déconnexion</a></li>
					        
					      </ul>
					    </div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>

				<div class="container col-md-4 col-md-offset-4"><h1 class ="align-center border-bottom">Ajouter une actualité</h1></div>
		</header>

		<section>
			<div style="display: none" class="alert alert-success container col-md-6 col-md-offset-3" id ="alert" role="alert">Les actualités ont bien était effacé</div>

				<div class="container col-md-8 col-md-offset-2 well">

					

      <form action="#" class="form-signin">
        <h3 class="form-signin-heading">Formulaire</h3>
        <label></label>
       <div class="">
         <label for='Titre'>Titre: </label>
        <label for="inputTitre" class="sr-only">Email address</label>
        <input type="Titre" id="inputitre" class="form-control" placeholder="Ecrire votre titre" required autofocus>
        <div class="">
         <label></label>
        <div class="">
         <label for='Soustitre'>Sous titre: </label>
        <label for="inputSousTitre" class="sr-only">Sous titre</label>
        <input type="Soustitre" id="inputSoustitre" class="form-control" placeholder="Ecrire votre sous titre" required>
         </div>
         <label></label>
           <div class="">
            <label for='Description'>Ajouter image: </label>
            <button class="btn btn-success" type="submit">Pièce jointe</button>
           </div>
         <label></label>
         <div class="">  
         <label for='Description'>Description: </label>
         <textarea class="form-control ckeditor" id="description" name='description' cols='50' rows='10' laceholder="Ecrire votre nouvelle" required></textarea>
        </div>
        <input class="btn btn-lg btn-success btn-block" type="submit" value="Publier" id="opener">

      </form>
				
				</div>

				  
		</section>
	</body>
</html>
	



