<!DOCTYPE HTML>
<html>
	<head>
		<title>Blog</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<link rel="stylesheet" href="scss/screen.css">
		<script src="bootstrap-sass-3.3.2/assets/javascripts/bootstrap/collapse.js"></script>
		<script src="bootstrap-sass-3.3.2/assets/javascripts/bootstrap/transition.js"></script>
		<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet"> 
		<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
	</head>

	<body>

		<header>
			<nav class="navbar navbar-default" id="navbar-menu">
				<div class="container-fluid">
					    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				    	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      	</button>
				      	<a class="navbar-brand" href="#">Menu</a>
				    </div>

				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      <ul class="nav navbar-nav">
				        <li class="active"><a href="index.php">Accueil <span class="sr-only">(current)</span></a></li>
				        <li><a href="upload.php">Ajouter une actualité</a></li>
				        <li><a href="connexion.php">Déconnexion</a></li>
				      </ul>
				    </div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>
			<div class="container col-md-4 col-md-offset-4" style='margin-top: 70px;'><h1 class ="align-center border-bottom">Liste des Actualités</h1></div>
		</header>



		<section class="container col-md-12">
			<div class="row-offcanvas row-offcanvas-left">
		  		<div id="sidebar" class="sidebar-offcanvas">
			    	<div class="col-md-12">
			        	<h3><img src='img/settings.png' id='img-nav'>Panneau de configuration</h3>
			        	<hr></hr>
			        	<ul class="nav nav-pills nav-stacked">
			          		<li><a href="#" id="del"><img src='img/delete.png' >  <span>Supprimer</span></a></li>
					          <li><a href="modify.php"> <img src='img/configure.png'>  <span class='margin-right'>Modifier</span></a></li>
					          <li><a href="#"> <img src='img/search.png' class = 'margin-right'><input type="text" class="form-control" placeholder="Rechercher" id='input-search'></a></li>
				        </ul>
		      		</div>
		  		</div>

		  		<div style="display: none" class="alert alert-success container col-md-6 col-md-offset-3" id ="alert" role="alert">Les actualités ont bien était effacé</div>

		  		<div class="container col-md-8 col-md-offset-2">
					<ul>
						<li class ="article">
							<div class ="header-article">
								<input type='radio' class = 'margin-left'>  <img src='img/configure.png' class = 'margin-right'>
								<input type='checkbox'>  <img src='img/delete.png' class = 'margin-right'>
								<img src='img/eye.png'> 
							</div>
							<h2 class="col-md-12">Titre de l'article</h2>
							<p  class="col-md-6"> Le Lorem Ipsum est simplement du faux texte employé dans la composition et
							 la mise en page avant impression. Le Lorem Ipsum est le faux texte standard
							 de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte.</p>
							
							<img class="col-md-4 col-md-offset-2"  id ='img-article' src='img/exemple.jpg'>
							
							
						</li>
					</ul>
				</div>

				<div class="container col-md-8 col-md-offset-2">
					<ul>
						</li>

						<li class ="article">

							<div class ="header-article">
								<input type='radio' class = 'margin-left'>  <img src='img/configure.png' class = 'margin-right'>
								<input type='checkbox'>  <img src='img/delete.png' class = 'margin-right'>
								<img src='img/eye.png'> 
							</div>
								<h2 class="col-md-12">Titre de l'article</h2>
								<p  class="col-md-6 "> Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte.</p>

								<img class="col-md-4 col-md-offset-2"  id ='img-article' src='img/exemple.jpg'>

								

						</li>
					</ul>
				</div>

				<div class="container col-md-8 col-md-offset-2">
					<ul>
						<li class ="article">
							<div class ="header-article">
								<input type='radio' class = 'margin-left'>  <img src='img/configure.png' class = 'margin-right'>
								<input type='checkbox'>  <img src='img/delete.png' class = 'margin-right'>
								<img src='img/eye.png'> 
							</div>
								<h2 class="col-md-12">Titre de l'article</h2>
								<p  class="col-md-6"> Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte.</p>

								<img class="col-md-4 col-md-offset-2"  id ='img-article' src='img/exemple.jpg'>
								
								
						</li>
					</ul>
				</div>
					<div class="container col-md-8 col-md-offset-2">
					<ul>
						<li class ="article">
							<div class ="header-article">
								<input type='radio' class = 'margin-left'>  <img src='img/configure.png' class = 'margin-right'>
								<input type='checkbox'>  <img src='img/delete.png' class = 'margin-right'>
								<img src='img/eye.png'> 
							</div>
								<h2 class="col-md-12">Titre de l'article</h2>
								<p  class="col-md-6"> Le Lorem Ipsum est simplement du faux texte employé dans la composition et
								 la mise en page avant impression. Le Lorem Ipsum est le faux texte standard
								 de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte.</p>
								
								<img class="col-md-4 col-md-offset-2"  id ='img-article' src='img/exemple.jpg'>
								
								
						</li>
					</ul>
				</div>
					<div class="container col-md-8 col-md-offset-2">
					<ul>
						<li class ="article">
							<div class ="header-article">
								<input type='radio' class = 'margin-left'>  <img src='img/configure.png' class = 'margin-right'>
								<input type='checkbox'>  <img src='img/delete.png' class = 'margin-right'>
								<img src='img/eye.png'> 
							</div>
								<h2 class="col-md-12">Titre de l'article</h2>
								<p  class="col-md-6"> Le Lorem Ipsum est simplement du faux texte employé dans la composition et
								 la mise en page avant impression. Le Lorem Ipsum est le faux texte standard
								 de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte.</p>
								
								<img class="col-md-4 col-md-offset-2"  id ='img-article' src='img/exemple.jpg'>
								
								
						</li>
					</ul>
				</div>
				<div class="container col-md-8 col-md-offset-2">
					<ul>
						<li class ="article">
							<div class ="header-article">
								<input type='radio' class = 'margin-left'>  <img src='img/configure.png' class = 'margin-right'>
								<input type='checkbox'>  <img src='img/delete.png' class = 'margin-right'>
								<img src='img/eye.png'> 
							</div>
								<h2 class="col-md-12">Titre de l'article</h2>
								<p  class="col-md-6"> Le Lorem Ipsum est simplement du faux texte employé dans la composition et
								 la mise en page avant impression. Le Lorem Ipsum est le faux texte standard
								 de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte.</p>
								
								<img class="col-md-4 col-md-offset-2"  id ='img-article' src='img/exemple.jpg'>
								
								
						</li>
					</ul>
				</div>
			</div>
			<nav class="container col-md-6 col-md-offset-5" id="pageNumbers">
			  <ul class="pagination">
			    <li class=""><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
			    <li class="active"><a href="#">1 <span class="sr-only"></span></a></li>
				<li class=""><a href="#">2 <span class="sr-only"></span></a></li>
				<li class=""><a href="#">3 <span class="sr-only"></span></a></li>
				<li class=""><a href="#">4 <span class="sr-only"></span></a></li>		
				<li class=""><a href="#">5 <span class="sr-only"></span></a></li>	    
				<li class=""><a href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
			  </ul>
			</nav>
		</section>

		<script>
			$('.btn-toggle').click(function() {
			    $(this).find('.btn').toggleClass('active');  
			    
			    if ($(this).find('.btn-primary').size()>0) {
			    	$(this).find('.btn').toggleClass('btn-primary');
			    }
			    if ($(this).find('.btn-danger').size()>0) {
			    	$(this).find('.btn').toggleClass('btn-danger');
			    }
			    if ($(this).find('.btn-success').size()>0) {
			    	$(this).find('.btn').toggleClass('btn-success');
			    }
			    if ($(this).find('.btn-info').size()>0) {
			    	$(this).find('.btn').toggleClass('btn-info');
			    }
			    
			    $(this).find('.btn').toggleClass('btn-default');
				       
			});

			$('form').submit(function(){
				alert($(this["options"]).val());
			    return false;
			});
			$('[data-toggle=offcanvas]').click(function() {
				$('.row-offcanvas').toggleClass('active');
			});

			$('#del').click(function(){
				//$("#dialog-confirm").show( "slow" );
				$( "#dialog-confirm" ).dialog({
			      resizable: false,
			      height:250,
			      width:500,
			      modal: true,
			      buttons: {
			        "Supprimer les actualités": function() {
			          $( this ).dialog( "close" );
			          $("#alert").show( "slow" );
			        },
			        Annuler: function() {
			          $( this ).dialog( "close" );
			        }
			      }
			    });
			});

		</script>
		<div style="display: none" id="dialog-confirm" title="Supprimer actualité?">
  		<p><span id="dialog" class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Vous être en train de supprimer des actualités. Etes-vous sur?</p>
		</div>
	</body>
</html>
	



