<html>
	<head>
		<title>Blog</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<link rel="stylesheet" href="scss/screen.css">
		<script src="bootstrap-sass-3.3.2/assets/javascripts/bootstrap/collapse.js"></script>
		<script src="bootstrap-sass-3.3.2/assets/javascripts/bootstrap/transition.js"></script>
	</head>

	<body>

		<div class="container page-header col-md-6 col-md-offset-3">
		<h1>Bienvenue dans la partie administration</h1><br>
		</div>

		<div class="container col-md-4 col-md-offset-4">

		<div class="panel panel-default">
		<div class="panel-heading"><h2> Se connecter</h2></div>
		<div class="panel-body">



		<form name='connexion' action="index.php" method='POST'>

			<div class="form-group">
				<label for='pseudo'>Pseudo : </label>
				<input type='text' name='pseudo' id='pseudo' class="form-control"  placeholder="Entrez le nom" required>
			</div>

			<div class="form-group">
			   	<label for='mdp'>Mot de passe : </label>
			   	<input type='password' name='mdp' id='mdp' class="form-control"  placeholder="Entrez le mot de passe" required>
			</div>

			<input type='submit' value='Se connecter' id='connecter' name='connecter'>

		</form>

	</body>
</html>
